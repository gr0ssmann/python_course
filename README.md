# python\_course

This is an introductory course in modern Python. This course is intended for self-study ("reverse classroom"). We use Jupyter, a user-friendly interface that allows running Python in the browser.

See below for instructions on how to install Python and Jupyter. Then, just download the notebook files and open them in Jupyter. Enjoy!

## Course contents

The Jupyter notebooks in this repository have the file name structure `course_XX_title.ipynb`, where `XX` derives from the number in this table of contents:

1. Arithmetic and other basics
1. Functions
1. Lists, sets and dictionaries
1. Classes
1. Control flow
1. List comprehensions etc.
1. Introduction to [numpy](https://numpy.org/)

## Installing Python and Jupyter

### Windows

### macOS

### GNU/Linux

Install Python 3.x and Jupyter using your distribution's package manager.

## Running Python and Jupyter

To peruse our lessons, run Jupyter by executing the following in your terminal:

```sh
jupyter-notebook
```

To run a Python program with the name `app.py`, run:

```sh
python app.py
```

To get an interactive Python shell after running the script, use:

```sh
python -i app.py
```

(On some platforms, you may need to replace `python` with `python3`.)

## License

Unless otherwise noted, everything in this repository is licensed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html), or (at your option) any later version.

This course is intended to be unencumbered by other people's intellectual property. When contributing to this repository, **do not use or even look at** other courses in Python. Your contributions are licensed as stated above. Use your common sense in determining your content and its structure. Moreover, this course follows the highest standards of pedagogy.

## Contributors

- [Max R. P. Grossmann](https://max.pm)
- Rosa Wolf
