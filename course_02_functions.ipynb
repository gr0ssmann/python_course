{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Functions\n",
    "\n",
    "Aim of this second Python lesson is to get familiar with functions. Let's take a moment and think about functions that we know from our maths classes, like $f(x) = x^2$. Once you \"assign\" an operation to a function $f()$, it is enough to write $f(x)$ to know which operation will be performed with the argument x you passed into the function. In Python, this function would be"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "\n",
    "def f(x):\n",
    "    return math.pow(x, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`def` is the keyword telling Python that the following is a function. `f` is the name of our function and, as in math, the `x` inside the parentheses is the function *argument*. \n",
    "\n",
    "After the colon we can define what the function should do with our parameter `x`, namely, it should `return` the squared value of `x`. This part of the function is called the *return statement*. A function can have zero or more return statements, but if a return statement is encountered, processing of the function terminates immediately.\n",
    "\n",
    "If you carefully read the documentation of `math` from the previous lesson, you know that `math.pow(x, 2)` is code executing what we would write mathematically as $x^2$. And just like in math, you can now put an abitrary value for `x` into the function to get its square. Now let's *call* the function with its name and enter some argument, for example 4:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f(4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just like in maths, you can also define a function that always returns the same value, e.g. 42. This function behaves like a constant."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def h(x):\n",
    "    return 42\n",
    "\n",
    "h(6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Although it appears useless at this point, it is also possible to define an \"empty\" function using the `pass` statement. This can be useful if you intend to \"fill\" the function later on. `pass` then behaves like a placeholder."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def k(x):\n",
    "    pass\n",
    "\n",
    "print(k(17))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you see, if you call a function that does not return anything, it is as if it returned `None`, a special value in Python."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Returning to our math examples, you can also create functions with several parameters. For example, you want to add `a` and `b` times `c`. Then, you can enter three abitrary numbers, e.g., 2, 4 and 8."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def g(a, b, c):\n",
    "    return a + b * c\n",
    "\n",
    "g(2, 4, 8)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, it is not possible to enter more or fewer arguments into a function than are *required*. The following two cells will not work."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g(1, 4, 5, 3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g(2, 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you only enter numbers into the function, the positioning of the arguments will determine which parameter they are processed as. In our example, the first positional argument is `a`, the second is `b` and the third is `c`. If you want to enter the arguments in a different order, you have to tell the function which specific parameter has which value. We call this method *calling a function with named arguments* to avoid confusion with another concept that we will be discussing shortly. However, sometimes the following is called *calling a function with keyword arguments*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g(c=2, a=1, b=3) #1 + 3 * 2 = 7 != 2 + 1 * 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, the functions in Python are much more universal than those you know from your math classes. They allow you to store and thus to execute anything you could do by hand. Indeed, Python functions can even return functions and construct entire classes! But we likely won't get that far.\n",
    "\n",
    "As a little fun fact, you already used functions in the first lesson of this course. `print()`, as well as `float()` are so-called **built-in functions** which are predefined and always available. [Here](https://docs.python.org/3/library/functions.html) you can find an overview of these functions.\n",
    "\n",
    "Let us now use a combination of a `string` and `print()` to define a function that greets anybody whose name we *pass* as argument. Say \"Hello\" to Harry!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def greet(name):\n",
    "    print(\"Hello \" + name)\n",
    "    \n",
    "greet(\"Harry\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So far, we only passed arguments directly to the function. Yet, you can also pass variables to a function. In this example, we define variable `another_name` and pass it to the function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "another_name = \"Hagrid\"\n",
    "    \n",
    "greet(another_name) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Magic! Speaking of magic, let's take a closer look at the universe of the people we have just got to know. Here we could use a function to state the magical status of a person. By default, we know that all students at Hogwarts are witches or wizards. So if we select any student, we know that he is a wizard. Therefore, we can define a **default** argument for the parameter in case we do not know the name of a male student. In this case, it is possible to call the function without an argument. So let's define such a function and find out what happens."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def who_is_who(names = \"The student from hogwarts\"):  #The default is \"The student from hogwarts\"\n",
    "    print(names + \" is a wizard.\")\n",
    "    \n",
    "who_is_who()    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we know a person's name, like Ron's, we can directly pass it to our function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "who_is_who(\"Ron\") "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Wait, what is with Hermione, Dobby, the Dursleys and all the others? Until now, we only had a single statement in our functions. It is also possible several statments in one function. For example, if we want to state the magic status of four characters simultaneously we can define a function to which we can pass different names."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def who_is_who(name1, name2, name3, name4):\n",
    "    print(name1 + \" is a wizard.\")\n",
    "    print(name2 + \" is a witch.\")\n",
    "    print(name3 + \" is a wizard.\")\n",
    "    print(name4 + \" is a muggle.\")\n",
    "\n",
    "who_is_who(\"Ron\", \"Luna\", \"Snape\", \"Dudley\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, you do not have to obey the order of the parameters when calling a function with named arguments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "who_is_who(name4=\"Petunia\", name2=\"Hermione\", name1=\"Harry\", name3=\"Dumbledore\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Functions are very powerful and it is possible to have functions with a variable number of arguments. You will often see something like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def some_function(argument1, *args, **kwargs):\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, to understand such a construct, you need to know about containers. Hence, we postpone this discussion to lesson 3. For now, you only need to know that such a function allows a variable number of arguments without names (stored in `args`) and with names (stored in `kwargs`)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, it is important to know that you can pass a return value of a function into another function without first storing it somewhere. Hence, you can do stuff like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f(f(f(3)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And you can call a function inside of itself. This is called *recursion* and can get difficult and slow quickly. You will see an example in lesson 5."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.2"
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
