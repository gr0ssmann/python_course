{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "40290abc",
   "metadata": {},
   "source": [
    "# Control flow"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9eb60582",
   "metadata": {},
   "source": [
    "Understanding control flow is one of the fundamental priorities when learning any programming language. Control flow allows you to run certain parts of your program multiple times and to skip certain parts in case they should sometimes not run. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6b22e9d1",
   "metadata": {},
   "source": [
    "## Functions and methods"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "752e399f",
   "metadata": {},
   "source": [
    "As you saw in lesson 2, custom functions offer a way to separate code from other code in such a way that you achieve modularity and avoid repetition. Hence, you might not be surprised that functions (and methods) are also important for control flow. Consider the following ([more infos here](https://en.wikipedia.org/wiki/Fibonacci_number)):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c5631849",
   "metadata": {},
   "outputs": [],
   "source": [
    "def fibonacci(x):\n",
    "    if x < 0:\n",
    "        return 0\n",
    "    elif x == 0 or x == 1:\n",
    "        return x\n",
    "    else:\n",
    "        return fibonacci(x - 1) + fibonacci(x - 2)\n",
    "\n",
    "print(f\"fibonacci(5) = {fibonacci(5)}\")\n",
    "print(f\"fibonacci(7) = {fibonacci(7)}\")\n",
    "print(f\"fibonacci(8) = {fibonacci(8)}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c89a1fa",
   "metadata": {},
   "source": [
    "When we define a function, nothing happens yet. Yet, when the Python interpreter reaches a line like\n",
    "\n",
    "```python\n",
    "print(f\"fibonacci(5) = {fibonacci(5)}\")\n",
    "```\n",
    "\n",
    "we can think of it \"jumping to\" the definition of `fibonacci(x)`, plugging in `x = 5` and then replacing `{fibonacci(5)}` with the result (`return`ed by the function).\n",
    "\n",
    "And so it \"jumps\" back and forth three times, for each invocation of `fibonacci(x)`. (Technically speaking, the Python interpreter does not truly jump, but that's just a technicality. But then again, it does execute many jump instructions, am I right? Nobody will understand that, I apologize.)\n",
    "\n",
    "Oh, and that also showed another cool feature that was recently (version 3.6 and above) added to Python: *f-strings*. Back in the old days, you would have had to write:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "301d17cd",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"fibonacci(5) = \" + str(fibonacci(5))) # first possibility\n",
    "print(\"fibonacci(5) = {}\".format(str(fibonacci(5)))) # another possibility"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1ebc5ebd",
   "metadata": {},
   "source": [
    "But these days, you should always use f-strings. f-strings look like strings, but before the first \", they have an `f`, and they may contain curly braces with variables or calls to functions. They are very powerful, but we will only cover the basics. Here are some examples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26cd1df5",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f\"{0.1234:.8f}\")\n",
    "\n",
    "name = \"Severin\"\n",
    "print(f\"Hi, {name}!\")\n",
    "\n",
    "print(f\"Something's missing with {name[1:]}.\") # remember this?\n",
    "\n",
    "import math\n",
    "print(f\"6^3 is {math.pow(6, 3)}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a7907f82",
   "metadata": {},
   "source": [
    "## If, elif, else\n",
    "These constructs allow you to run code only if certain conditions are met. For example, here you can enter a word and the program will congratulate you if it is a palindrome:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c5e7b80c",
   "metadata": {},
   "outputs": [],
   "source": [
    "word = \"racecar\" # try also lagerregal\n",
    "\n",
    "if word == word[::-1]:\n",
    "    print(\"CONGRATULATIONS!\")\n",
    "else:\n",
    "    print(f\"{word} does not equal {word[::-1]}!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2f2cf09c",
   "metadata": {},
   "source": [
    "Pretty cool, isn't it?\n",
    "\n",
    "Note the difference between `=` (line 1) and `==` (line 3). The first one assigns a value, the second one *checks* whether two values are equal in the eyes of Python.\n",
    "\n",
    "However, our program above quickly runs into trouble. Try `word = \"Racecar\"` and see why. Maybe we want to warn users if they put in uppercase characters. In such cases, we can put an `elif` condition between `if` and `else`. Note: Only the `if` is ever mandatory, but if you use `elif` and/or `else`, you must always use them in exactly this order. You can have multiple `elif`s. Regarding our problem, strings have the useful method `islower`. Let's use it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "57fa006b",
   "metadata": {},
   "outputs": [],
   "source": [
    "word = \"Racecar\"\n",
    "\n",
    "if word == word[::-1]:\n",
    "    print(\"CONGRATULATIONS!\")\n",
    "elif not word.islower():\n",
    "    print(\"Please do not use uppercase characters.\")\n",
    "else:\n",
    "    print(f\"{word} does not equal {word[::-1]}!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7a2eb35a",
   "metadata": {},
   "source": [
    "In line 5, we check whether the word does *not* entirely consist of uppercase characters, and if so, line 6 is executed. Note that the `if` construct always branches, i.e. if `elif` is triggered, `else` will not be executed and so on. This (badly written) program illustrates this issue:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "029bc150",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 5\n",
    "\n",
    "if x > 0:\n",
    "    print(\"The number is strictly positive\")\n",
    "elif x == 5:\n",
    "    print(\"The number is five\")\n",
    "elif x%2 == 1:\n",
    "    print(\"The number is odd\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6506bf02",
   "metadata": {},
   "source": [
    "Clearly, all conditions are `True`, but only the first `True` one leads to an execution of code. You must at all times be aware of this issue and structure your code such that all conditions are disjoint."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c89b3e1c",
   "metadata": {},
   "source": [
    "**Regarding the lowercase/uppercase issue, can you think of a better way to solve this problem?**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2c87477",
   "metadata": {},
   "source": [
    "## Loops"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "supreme-packet",
   "metadata": {},
   "source": [
    "### for"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ee56452",
   "metadata": {},
   "source": [
    "Young Carl Friedrich Gauss (a mathemagician of the highest order) once got into a lot of trouble as a kid, and as punishment had to sum all integers from 1 to 100. To his teachers dismay, he calculated the value immediately in his head. Had he had Python, he might have been even faster:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5b5e64d7",
   "metadata": {},
   "outputs": [],
   "source": [
    "sum(range(101))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ee4f4210",
   "metadata": {},
   "source": [
    "But what does `sum` actually do? Well, fundamentally, it just goes *through each element* of an *iterable* (often: a *list*) and adds their values one by one. We can do that, too, using Python's `for` loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "736cbe60",
   "metadata": {},
   "outputs": [],
   "source": [
    "def our_sum(values):\n",
    "    s = 0\n",
    "    \n",
    "    for val in values:\n",
    "        s += val\n",
    "    \n",
    "    return s"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f416e516",
   "metadata": {},
   "outputs": [],
   "source": [
    "our_sum(range(101))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af1c6c3b",
   "metadata": {},
   "source": [
    "Works! As you see in line 4, `for` is a loop that goes through each element of an iterable. Within the `for` block, `val` is replaced by the *current* value in `values`, and the block is executed until the iterable is exhausted (e.g. if we went through the whole list).\n",
    "\n",
    "If you ever want to execute some piece of code multiple times, you could do the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "controlling-action",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "for i in range(5):\n",
    "    print(\"I will be printed five times.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "isolated-complex",
   "metadata": {},
   "source": [
    "Note how we don't really need the variable `i`. Typically, this is resolved by renaming an unnecessary variable to `_`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "false-fourth",
   "metadata": {},
   "outputs": [],
   "source": [
    "for _ in range(5):\n",
    "    print(\"I will be printed five times.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "completed-underground",
   "metadata": {},
   "source": [
    "Using `zip` (see lesson 3), we could easily calculate the *inner product* of two `list`s."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "892b259a",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = [2, 4, 7]\n",
    "y = [-1, 3, 2]\n",
    "\n",
    "ip = 0\n",
    "\n",
    "for current1, current2 in zip(x, y):\n",
    "    ip += current1*current2\n",
    "\n",
    "print(ip)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2622d70",
   "metadata": {},
   "source": [
    "To \"unpack\" what is going on here, let's take a look at what `zip` actually does:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ca5216d9",
   "metadata": {},
   "outputs": [],
   "source": [
    "list(zip(x, y))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90af695a",
   "metadata": {},
   "source": [
    "OK, so it pairs each value from `x` with its corresponding value from `y`. Hence, when *iterating* through each value of the *iterable* `zip(x, y)`, a pair of values (a tuple with two elements) is actually returned! And hence, we *unpack* it into two by using a comma. If we didn't do that, the following would happen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d21cbcb7",
   "metadata": {},
   "outputs": [],
   "source": [
    "for current_pair in zip(x, y):\n",
    "    print(current_pair)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c9041c70",
   "metadata": {},
   "source": [
    "Of course, we could do this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ff0fee10",
   "metadata": {},
   "outputs": [],
   "source": [
    "ip = 0\n",
    "\n",
    "for current_pair in zip(x, y):\n",
    "    ip += current_pair[0]*current_pair[1]\n",
    "\n",
    "print(ip)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d715a7ba",
   "metadata": {},
   "source": [
    "... but this is seen as less elegant. If we always expect a tuple (see lesson 3) of fixed length, you should unpack it into several variables when iterating. It's most readable that way."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "human-opera",
   "metadata": {},
   "source": [
    "### while"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "respected-hours",
   "metadata": {},
   "source": [
    "Python has another kind of loop that is similar to `if`. `while` executes a given piece of code as long as the condition given to `while` remains true. For example, the following cell prints the squares of all positive numbers until 11:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "statistical-animation",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "x = 0\n",
    "\n",
    "while x <= 11:\n",
    "    print(math.pow(x, 2))\n",
    "    x += 1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "affiliated-morning",
   "metadata": {},
   "source": [
    "In this case, you would probably want to use a `for` loop. In general, `while` loops are used if the loop should terminate on a condition more complicated than a simple iteration. That's because you could write the code above much more elegantly and concisely as"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "signed-center",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "for x in range(12):\n",
    "    print(math.pow(x, 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "accredited-prayer",
   "metadata": {},
   "source": [
    "It is occasionally useful to set the condition to `True` and have the `while` loop terminate when a condition determined inside of the loop is reached. To terminate the loop in such a \"manual\" way requires the use of `break`. For example, the following more complicated example generates random numbers until one is found that is divisible by 7:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aging-artwork",
   "metadata": {},
   "outputs": [],
   "source": [
    "import random\n",
    "\n",
    "while True:\n",
    "    my_number = random.randint(0, 1000)\n",
    "    \n",
    "    print(f\"Checking {my_number}...\")\n",
    "    \n",
    "    if my_number2 % 7 == 0:\n",
    "        break\n",
    "\n",
    "print(f\"Found one: {my_number}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "indoor-hungary",
   "metadata": {},
   "source": [
    "If you run the previous cell multiple times, you will get different results.\n",
    "\n",
    "In general, you find much fewer `while` loops in Python than in other languages. That's because `for` is just so epic. Overuse of `while` is one way to spot beginners. Nonetheless, `while` can sometimes be useful, especially as `while True`."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
