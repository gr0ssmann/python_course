{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "056c5588",
   "metadata": {},
   "source": [
    "# Lists, sets and dictionaries"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5b34daff",
   "metadata": {},
   "source": [
    "In this lesson, we will consider *containers*. In Python, containers (or \"collections\") have quite a lot in common with the containers you may know from this planet's most distinguished ports and perhaps even your own home. The containers we will talk about here are able to store data of many different *types* (remember these?)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4b249f70",
   "metadata": {},
   "source": [
    "## Tuples"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf7950fd",
   "metadata": {},
   "source": [
    "Before we get started with more sophisticated containers, let's look at a very basic one. It is *immutable*, meaning its makeup and its elements cannot be changed after creation. Tuples are enclosed in parentheses. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "234792ef",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_tuple = (1, 4, 9, 16, 25, 36)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "487df49a",
   "metadata": {},
   "source": [
    "Like all of the basic Python containers, tuples can contain elements of various types."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a9969da3",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_tuple2 = (1, 2, 3, 5, 7, \"prime\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ddbe348b",
   "metadata": {},
   "source": [
    "And you can access elements with square brackets:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "51bf53b8",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_tuple[4]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba49d7b8",
   "metadata": {},
   "source": [
    "We explain below why this did not return `16`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eff99af3",
   "metadata": {},
   "source": [
    "## Lists"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba45218b",
   "metadata": {},
   "source": [
    "The most important type of container is a `list`. A `list` is a built-in Python class that is simply an arrangement of objects. The objects don't all have to be of the same type. A list can be constructed like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "949845fb",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_list = [4, 9, 6, 8, 17]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e7976584",
   "metadata": {},
   "source": [
    "Now that we have saved the list in a variable called `my_list`, we can access individual items like so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "870132ee",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_list[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d7b8a58f",
   "metadata": {},
   "source": [
    "Isn't that odd? Shouldn't that have returned `4`? No. Python follows the convention that the *first* element of a list has index 0, and so on. Hence, with `my_list[1]`, you actually queried for the *second* element!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c57b56d",
   "metadata": {},
   "source": [
    "An important feature of lists is that they are mutable. This allows you to build up a list gradually, as you go along. (This might not be the best way to do it, however. In lesson 6, you will find out about one particular more *pythonic* method.) To staple one element to an existing list, use `my_list.append`: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1c56e88d",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_list.append(-4)\n",
    "my_list.append(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4d18a975",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_list"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "581ea90e",
   "metadata": {},
   "source": [
    "Pretty neat, huh?\n",
    "\n",
    "We call `append` a *method* of `my_list`. You will learn more about methods in lesson 4. For now, all you need to know is that each `list` comes with its very own set of functions that change the makeup of the `list`. Another great method is `sort`. But before we try that, let's talk about a crucial distinction that often is very relevant. Take a look at this: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "706444eb",
   "metadata": {},
   "outputs": [],
   "source": [
    "sorted(my_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "503152ec",
   "metadata": {},
   "source": [
    "OK, so this gives you the sorted list. What is the difference to `my_list.sort()`? It's very simple, but incredibly important to remember.\n",
    "\n",
    "**`sorted(my_list)` gives you a new, but sorted, list. `my_list.sort()` sorts the existing list.**\n",
    "\n",
    "In other words: `sorted(my_list)` creates a new list, while `my_list.sort()` changes the list `my_list`. You can remember this not only because the words give you a clue, but because `my_list.sort()` looks more like it operates on `my_list` itself (it is a method of `my_list` after all). There are many nuances of this sort in programming and you need to know subtleties like this.\n",
    "\n",
    "Now that we have stated the difference, let us run the \"other\" sort:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e3adbd5a",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_list.sort()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2c996447",
   "metadata": {},
   "source": [
    "As you see, nothing is returned because that would be pointless. The result is already stored in the existing list `my_list`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "62ecb8a7",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_list"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "031a04f5",
   "metadata": {},
   "source": [
    "OK, it's certainly sorted!\n",
    "\n",
    "A function that operates on all containers and many other objects is `len()`. It returns the number of objects. As you will see in lesson 5, there is actually scant reason to use `len()` in Python, at least if you want to write clean code. Nonetheless, this function is very useful in certain instances:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "82cdda27",
   "metadata": {},
   "outputs": [],
   "source": [
    "len(my_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "afb387cc",
   "metadata": {},
   "source": [
    "You can clear a list as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "849495f4",
   "metadata": {},
   "outputs": [],
   "source": [
    "bad_list = [19, 17]\n",
    "bad_list.clear()\n",
    "bad_list"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d014b8a5",
   "metadata": {},
   "source": [
    "To remove a particular *value*, you can do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "abd1799a",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_list.remove(-4)\n",
    "my_list"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79fef3f7",
   "metadata": {},
   "source": [
    "As you see, `-4` was removed even though it appeared twice. Pretty neat! To delete an element at a specific index, you can do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f5c28316",
   "metadata": {},
   "outputs": [],
   "source": [
    "del my_list[2]\n",
    "my_list"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c6c179ca",
   "metadata": {},
   "source": [
    "To remove the last element of a `list`, there is `my_list.pop`, which has the awesome bonus that it returns the value that was just removed. In many cases where a `list` is somehow gradually \"consumed\", use of this method turns out to be an extremely clean way to do it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "27a27e7d",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(my_list.pop())\n",
    "my_list"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "52fe84a9",
   "metadata": {},
   "source": [
    "Let's talk about *slices* now, but before we do that, let's fill up a list with the index of each element. Don't worry about what happens here; `range()` returns a special kind of object that is *not* a list, and wrapping this result in `list()` makes it into a list. Enough talk:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a99a673",
   "metadata": {},
   "outputs": [],
   "source": [
    "cool_list = list(range(12))\n",
    "cool_list"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb1a8f73",
   "metadata": {},
   "source": [
    "That's right, `range(x)` goes from 0 to `x-1`. That's just how it is.\n",
    "\n",
    "A slice is something like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a2be84e",
   "metadata": {},
   "outputs": [],
   "source": [
    "cool_list[:4]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2f1aec56",
   "metadata": {},
   "source": [
    "As you can see, this returns the first four elements. This is because we elided the *start index* in the slice and only specified the *stop index*. More precisely, a slice looks like this:\n",
    "\n",
    "```python\n",
    "list[start:stop:step]\n",
    "```\n",
    "\n",
    "Both `start` (default: 0) and `step` (default: 1) are optional, and if you skip `step`, it suffices to use one `:` (biologists call this a \"colon\"). You could do any of the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dfb0baa7",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(cool_list[:8:])\n",
    "print(cool_list[:8])\n",
    "print(cool_list[3:8])\n",
    "print(cool_list[3:8:2])\n",
    "print(cool_list[::2])\n",
    "print(cool_list[2::3])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7e0b4981",
   "metadata": {},
   "source": [
    "You see how slices allow you to access particular values in very convenient ways. Perhaps it is important to note that this point that indices can be *negative*. Yup! In that case, they count from the other end of the `list`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f9eeea9c",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(cool_list[-1])\n",
    "print(cool_list[-2])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "34f2e3e3",
   "metadata": {},
   "source": [
    "If you were very attentive and engaged with the class, you might have noticed that each `list` has a method called `reverse`. This method permanently reverses a list. Can you think of a way to see the reverse of a `list` without changing it (and without using `reversed()`)? I sure can:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1578d07c",
   "metadata": {},
   "outputs": [],
   "source": [
    "cool_list[::-1]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ca2c675d",
   "metadata": {},
   "source": [
    "That's massively epic!\n",
    "\n",
    "By the way, you can check whether a list contains some value using the `in` operator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "94621299",
   "metadata": {},
   "outputs": [],
   "source": [
    "13 in cool_list"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b79f6e88",
   "metadata": {},
   "source": [
    "As you saw in lesson 2, `True` and `False` are boolean values. So, this construct works pretty much as expected!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c2e95e1a",
   "metadata": {},
   "source": [
    "## Sets"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7cdc2d2a",
   "metadata": {},
   "source": [
    "Sets are similar to lists, but they have a few interesting features:\n",
    "\n",
    "1. They can only contain elements that can be compared to each other, but they need not be of the same type. (This type of comparability is called \"hashability\" and if you use simple types, you need not worry about it.)\n",
    "2. **Each element is contained at most one time.**\n",
    "3. You cannot access individual elements by index. Sets don't have a true order.\n",
    "4. Sets are usually constructed from another container.\n",
    "\n",
    "Let's check it out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a91f7074",
   "metadata": {},
   "outputs": [],
   "source": [
    "many_duplicates = [2, 3, 4, 2, 3, 4, 5, 1, 2, 3, 4]\n",
    "my_set = set(many_duplicates)\n",
    "my_set"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5dad534e",
   "metadata": {},
   "source": [
    "See how we fed the existing list `many_duplicates` into `set()`? That's pretty classic. And sure enough, the duplicates are gone with the wind!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b275ae26",
   "metadata": {},
   "source": [
    "Sets can save you a lot of work. If you want to add something to a `set`, it will automatically check whether the element exists already. Oh, and perhaps we should mention that **sets are wicked fast** because of extremely high-level scientific reasons. We recommend: Anytime you expect to have no duplicates, consider using a `set`. To recap:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f54e555b",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_set.add(5) # already in there\n",
    "my_set.add(9) # new\n",
    "\n",
    "my_set"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3cdbf3d0",
   "metadata": {},
   "source": [
    "Of course, you can always convert a `set` back into a `list`. By the way, the following works pretty much always, and is recommended in functions where you expect an argument to be a list to make sure it's a list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4e92dfb0",
   "metadata": {},
   "outputs": [],
   "source": [
    "list(my_set)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ceb3523e",
   "metadata": {},
   "source": [
    "However, such a conversion is often not necessary. As you will learn in lesson 5, you can still iterate over a `set`, so the fact that you cannot access individual elements does not matter much in practice.\n",
    "\n",
    "If you have two sets, you can do high-level mathemagics, like create a *union* or *intersection* or a set difference. Just as an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e8230909",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_set2 = set([3, 7, 9])\n",
    "my_set.intersection(my_set2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3dbf453",
   "metadata": {},
   "source": [
    "Pretty epic! By the way, a union is also pretty simple:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0acadcab",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_set.union(my_set2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68a72178",
   "metadata": {},
   "source": [
    "... set difference:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "544bf456",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_set.symmetric_difference(my_set2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "98a8031b",
   "metadata": {},
   "source": [
    "... and there are many more methods. Please check them out!\n",
    "\n",
    "Oh, and:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0b8af74d",
   "metadata": {},
   "outputs": [],
   "source": [
    "2 in my_set"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b28a484d",
   "metadata": {},
   "source": [
    "## Dictionaries"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d2100aa",
   "metadata": {},
   "source": [
    "Dictionaries allow you to save *key-value pairs*. Previously, indices needed to be integers; with dictionaries, they can be any hashable (read: comparable) type. This is especially useful in conjunction with strings. There are two popular methods of building dictionaries. The first one is to do this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22a4cdac",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_dict1 = {'key1': 17.4, 'key2': -5, 'abc': 'hi'}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cfdb253b",
   "metadata": {},
   "source": [
    "Another popular method is to do"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "46468e9d",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_dict2 = dict(key1=17.4, key2=-5, abc='hi')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8e08f116",
   "metadata": {},
   "source": [
    "In general, we prefer the first method. That is because it is more terse and allows all possible keys, while the second method only allows string keys that fit into the names of function arguments. Indeed, with the first method,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8687930c",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_dict3 = {6.3: 'G', 9.2: 'M', 'yolo': 0}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2169482b",
   "metadata": {},
   "source": [
    "Pretty crazy, huh? It's true: The keys can be almost anything. And it works just fine:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "289c1fc3",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_dict3[9.2]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8ff8554",
   "metadata": {},
   "source": [
    "Of course, dictionaries present some unique challenges. Obviously, each key must be unique. But how can you check if a certain `dict` already contains a key? Very simple:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b10a0865",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(6.3 in my_dict3)\n",
    "print('key3' in my_dict2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6a6f1eaf",
   "metadata": {},
   "source": [
    "So this pretty much works as with sets and lists, but on the keys!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3b5d9d6",
   "metadata": {},
   "source": [
    "## Concatenating two or more containers"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d9452ab6",
   "metadata": {},
   "source": [
    "To concatenate means to put together. We already saw how to concatenate two sets (via `union`), so let's look at lists. It's actually trivial:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "94b01936",
   "metadata": {},
   "outputs": [],
   "source": [
    "list1 = [4, 3, 'test']\n",
    "list2 = [-0.1, 'foo']\n",
    "\n",
    "list1 + list2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "48d55533",
   "metadata": {},
   "source": [
    "And that's why people like Python.\n",
    "\n",
    "But wait... with dictionaries, unfortunately, it is not that simple. In the most recent versions of Python (Python 3.9 and above), you can do this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ad9dc4de",
   "metadata": {},
   "outputs": [],
   "source": [
    "dict1 = {'a': 7, 'b': -5}\n",
    "dict2 = {'g': -2, 'h': 1}\n",
    "\n",
    "dict1 | dict2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e5c36fb9",
   "metadata": {},
   "source": [
    "... but often, we are still required to do this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4f83cda4",
   "metadata": {},
   "outputs": [],
   "source": [
    "dict3 = {}\n",
    "\n",
    "dict3.update(dict1)\n",
    "dict3.update(dict2)\n",
    "\n",
    "dict3"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b37dc321",
   "metadata": {},
   "source": [
    "Both methods are completely fine, and indeed the older version may be argued to be more expressive. And indeed, there is a third way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2e7e3ee2",
   "metadata": {},
   "outputs": [],
   "source": [
    "{**dict1, **dict2}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af520dd0",
   "metadata": {},
   "source": [
    "... and:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "da12baef",
   "metadata": {},
   "outputs": [],
   "source": [
    "dict(dict1, **dict2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9b9bc22b",
   "metadata": {},
   "source": [
    "Python truly is the land of possibilities."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58a29ba9",
   "metadata": {},
   "source": [
    "## Useful functions that work on containers"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c46be1e",
   "metadata": {},
   "source": [
    "Python has many, *many* functions that work on containers. We already covered `sorted()`, but there is also `sum`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b4ce2489",
   "metadata": {},
   "outputs": [],
   "source": [
    "sum(my_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "de30ebed",
   "metadata": {},
   "source": [
    "Or, for data science:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2a7fab2",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(min(my_list))\n",
    "print(max(my_list))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "da43e422",
   "metadata": {},
   "source": [
    "One of the most epic functions in Python is `zip()`. `zip` allows you to walk through two or more containers simultaneously. For example, let's say you have these two lists:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "092ba1f9",
   "metadata": {},
   "outputs": [],
   "source": [
    "names = ['Amelia', 'Bob', 'Cecilia', 'Donald']\n",
    "ages = [29, 40, 25, 76]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "89b40651",
   "metadata": {},
   "source": [
    "`zip` returns a special object - a so-called \"iterable\" - that represents pairs of elements at the same position. To see what is going on, let's convert the result from `zip` into a `list`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c761c0a6",
   "metadata": {},
   "outputs": [],
   "source": [
    "list(zip(names, ages))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0887511b",
   "metadata": {},
   "source": [
    "Huh! As you see, `zip` combined each element from `names` with its corresponding element at `ages`. This will turn out to be very important for lesson 5. The use of functions like `zip` is important if you wish your code to be considered a true pythonic masterpiece."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c247b09",
   "metadata": {},
   "source": [
    "## Using containers for functions with variable arguments"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "45016311",
   "metadata": {},
   "source": [
    "As promised in lesson 2, here are some details on functions that look like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9d4d4186",
   "metadata": {},
   "outputs": [],
   "source": [
    "def some_function(argument1, *args, **kwargs):\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5bd89b16",
   "metadata": {},
   "source": [
    "Let's change the function somewhat to see what this does."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9d826ea4",
   "metadata": {},
   "outputs": [],
   "source": [
    "def some_function(argument1, *args, **kwargs):\n",
    "    print(\"argument1:\", argument1)\n",
    "    print(\"args:\", args)\n",
    "    print(\"kwargs:\", kwargs)\n",
    "\n",
    "some_function(42, 19, 38, omega=0.1, z=6.9)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7710c4d9",
   "metadata": {},
   "source": [
    "The argument `argument1` is mandatory. However,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e0271e73",
   "metadata": {},
   "outputs": [],
   "source": [
    "some_function(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9b650684",
   "metadata": {},
   "source": [
    "... works! `*args` and `*kwargs` \"catch\" all extra arguments passed to the function. In the tuple `*args`, you will find *(non-named) positional arguments*; you will find them in the order they were passed. And the dictionary `*kwargs` contains *(named) keyword arguments*. Oh, hey ... do you remember when we constructed a `dict` like this?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35c565a6",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_dict2 = dict(key1=17.4, key2=-5, abc='hi')\n",
    "my_dict2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef6b4c4f",
   "metadata": {},
   "source": [
    "I 'member. Turns out this could be implemented quite trivially:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c8c67a33",
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_me_a_dict(**kwargs):\n",
    "    return kwargs\n",
    "\n",
    "my_dict3 = make_me_a_dict(key1=17.4, key2=-5, abc='hi')\n",
    "my_dict3"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "904400e4",
   "metadata": {},
   "source": [
    "Python is truly a wonderland. Not so wintery, though."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
