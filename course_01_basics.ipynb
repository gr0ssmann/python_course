{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "sitting-program",
   "metadata": {},
   "source": [
    "# Arithmetic and other basics\n",
    "Welcome to this class in Python! We're glad you're here.\n",
    "\n",
    "In this first lesson, we will learn how to use Python as a calculator and to do very basic things.\n",
    "\n",
    "There are two main types of cell in a Jupyter Notebook. This cell is a \"Markdown\" cell, and it allows us to write funny little words that humans can read. However, there are also code cells that you can execute. The neat thing about Jupyter is how it allows you to run code in your *browser*! Click the following cell and press Ctrl+Enter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "sacred-district",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Hi there!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "marked-glossary",
   "metadata": {},
   "source": [
    "**In this course, we expect you to do that with all cells that have an \"In\" on the left.**\n",
    "\n",
    "As you saw, Python wrote \"Hi there!\" on the screen. Pretty cool, huh? However, in this introductory class, most cells will be self-contained or at least they will only have a single output. By default, if no `print()` is used, Jupyter prints out the last result. As an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "comparable-orbit",
   "metadata": {},
   "outputs": [],
   "source": [
    "7-3\n",
    "1+1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "planned-parade",
   "metadata": {},
   "source": [
    "**Watch what happens if you remove the second line of the previous cell.**\n",
    "\n",
    "**Watch what happens if you wrap both lines in print().**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dedicated-calcium",
   "metadata": {},
   "source": [
    "Congratulations! You have successfully run your first lines of Python. Python is a very powerful programming language. It is also so friendly to humans that it is often called a \"high level language\", because you can deal with tremendous abstractions. We won't get that far today, so let's instead do some arithmetic - or as I have taken to call it, \"arithmagic\"."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "tamil-vertical",
   "metadata": {},
   "source": [
    "We already saw addition. How about ... multiplication? Very simple:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "tracked-significance",
   "metadata": {},
   "outputs": [],
   "source": [
    "3*4 # this is twelve "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "british-reason",
   "metadata": {},
   "source": [
    "Python is pretty smart, isn't it? Oh, and did you notice the words after the \"hashtag\" (actually called the \"pound symbol\")? That's right: It's a **comment**, something that should be added frequently into programs. Everything after a hashtag is ignored in Python. But it ain't ignored by humans, let me tell ya!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "radio-nelson",
   "metadata": {},
   "outputs": [],
   "source": [
    "3/4"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "quality-commissioner",
   "metadata": {},
   "source": [
    "As you saw just now, Python can easily deal with numbers that are not whole. (We sometimes call whole numbers \"integers\", a term some verious serious scientists came up with.) This leads to the issue of **types**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "reasonable-aruba",
   "metadata": {},
   "source": [
    "Python is one of the more liberal languages out there, but it is often reasonable to be a bit more restrained. Hence, we need to distinguish between things like\n",
    "\n",
    "```python\n",
    "\"3\"\n",
    "```\n",
    "\n",
    "and\n",
    "\n",
    "```python\n",
    "3\n",
    "```\n",
    "\n",
    "Check this out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fresh-nashville",
   "metadata": {},
   "outputs": [],
   "source": [
    "\"3\"*4 # what do you expect is this cell's output?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "spiritual-brake",
   "metadata": {},
   "source": [
    "Hold the fort, Joe, what just happened there? Very simple: Everything enclosed in double quotes (\") or single quotes (') has the type **string**. A `string` is just a particular combination of characters. Indeed, in the very first cell that we ran - remember? - the words \"Hi there!\" were enclosed in a `string`.\n",
    "\n",
    "And Python has the neat feature that strings can be multiplied by an `int` - they are just repeated in that case. Wow! Of course, you cannot divide a string, at least not using arithmetic operators.\n",
    "\n",
    "**Watch what happens if you multiply a string by 0.**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "taken-system",
   "metadata": {},
   "source": [
    "In Python, integers are called **int**. We can often convert from one type to another. Let's see what happens if we convert the string \"12\" to an `int`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "exclusive-circus",
   "metadata": {},
   "outputs": [],
   "source": [
    "int(\"12\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "prescribed-throw",
   "metadata": {},
   "source": [
    "That's right! No more quotes - it's an `int` now. In Python, numbers that are allowed to be not whole are called **float**. The type `float` is somewhat similar to real numbers, although there are many exceptions. Most importantly, `float` does not have infinite resolution. What is `0.3-0.2`, you ask? Very simple:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "exterior-flour",
   "metadata": {},
   "outputs": [],
   "source": [
    "0.3-0.2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "governmental-denmark",
   "metadata": {},
   "source": [
    "What? No! On the other hand... almost. This is one of the consequences of how the computer encodes `float`. [See this video](https://www.youtube.com/watch?v=PZRI1IfStY0) for more information. That's just how it is - if you need more precision, you must use something like Python's own `decimal.Decimal`, but we won't cover that here.\n",
    "\n",
    "Let's do some more fun arithmagic:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "integral-enforcement",
   "metadata": {},
   "outputs": [],
   "source": [
    "21/0.5"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fatal-governor",
   "metadata": {},
   "source": [
    "As you see, the result of a division of an `int` by a `float` is always a float, and the reverse is also true - even if it's not necessary. *Them's the rules...*"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "happy-building",
   "metadata": {},
   "source": [
    "It's very simple to store a value so that it can be used later in a program. This is called an **assignment**. Consider this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "nominated-british",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_variable = 19.3"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "delayed-modem",
   "metadata": {},
   "source": [
    "This cell doesn't give any output because an assignment by itself never produces output. If you executed the previous cell, then the following will work:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "hungry-attack",
   "metadata": {},
   "outputs": [],
   "source": [
    "17*my_variable - 5"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "accompanied-international",
   "metadata": {},
   "source": [
    "**Watch what happens if you change my_variable and re-run the previous cell.**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "relevant-draft",
   "metadata": {},
   "source": [
    "Oh, something else that's very important. Except at the beginning of a line and inside of strings, *whitespace* does not matter. We could have written the previous cell like this and we wouldn't have had any trouble whatsoever:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "developing-austria",
   "metadata": {},
   "outputs": [],
   "source": [
    "17*my_variable-5"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "hybrid-shopping",
   "metadata": {},
   "source": [
    "It's often recommended to follow some style rules. We don't like doing that, however, and for our own projects, we use an awesome little software called [black](https://github.com/psf/black) that does all of this work for us. That's just our preference, but it should be yours!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "turkish-ending",
   "metadata": {},
   "source": [
    "It's perhaps obvious, but Python can do very powerful calculations. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "absent-gather",
   "metadata": {},
   "outputs": [],
   "source": [
    "27*(-5*2/(18-5*(9/6 - my_variable)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "announced-advice",
   "metadata": {},
   "source": [
    "Python is worth it, even if it's just as a calculator. Let's step out of arithmetic for a second and talk about a few mathematical functions that are often useful. First of all, there's `round`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "original-breed",
   "metadata": {},
   "outputs": [],
   "source": [
    "round(1.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "governing-recording",
   "metadata": {},
   "source": [
    "How about..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "rolled-narrow",
   "metadata": {},
   "outputs": [],
   "source": [
    "round(0.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "composite-struggle",
   "metadata": {},
   "source": [
    "Wait a minute ... shouldn't that be 1? Depends on your perspective! Python uses a special method of rounding that is called [bankers' rounding](https://en.wikipedia.org/wiki/Rounding#Round_half_to_even) and that was specified in an international standard by very important high-level engineers. It's superior to the usual way of rounding numbers because being stuck halfwhere between two integers is pretty common, and always rounding this up can lead to severe problems in calculations. This is one of the examples where Python took a different route than most other programming languages - you should know things like that, or you will learn the hard way. If you want to round down, there is `floor` and if you want to round up, there is `ceil`. Both of *these* function are in the `math` module, which you first need to import. And you need to call these functions in a special way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "strategic-destiny",
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "\n",
    "math.ceil(19.2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "desperate-bridge",
   "metadata": {},
   "source": [
    "`math` contains many other useful functions. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "terminal-malpractice",
   "metadata": {},
   "outputs": [],
   "source": [
    "math.sqrt(9)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "negative-meter",
   "metadata": {},
   "source": [
    "That's the square root. Or we have the exponential function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "plastic-draft",
   "metadata": {},
   "outputs": [],
   "source": [
    "math.exp(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "overall-transport",
   "metadata": {},
   "source": [
    "In science - our realm - this number is pretty important. In any case, if you want to inspect all there is in `math`, you have two possibilities:\n",
    "\n",
    "1. You can go to `math`'s [website](https://docs.python.org/3/library/math.html).\n",
    "2. You can create a new cell in this notebook, enter the following, go to the end of the line after the period and press the Tab button on your keyboard:\n",
    "```python\n",
    "math.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "secret-superintendent",
   "metadata": {},
   "source": [
    "Of course, you can feed the output of one function into another function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "lucky-philosophy",
   "metadata": {},
   "outputs": [],
   "source": [
    "math.log(math.exp(4.3))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "abroad-forest",
   "metadata": {},
   "source": [
    "How quaint!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dressed-wagon",
   "metadata": {},
   "source": [
    "By the way, it can be useful to import some function (or any object in general) from a module into the current namespace so that calls to it need not be prefixed by `math.`. This can be done like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "perfect-answer",
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import pi"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "lasting-germany",
   "metadata": {},
   "source": [
    "Which gives rise to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "vietnamese-maryland",
   "metadata": {},
   "outputs": [],
   "source": [
    "r = 5.5 # radius of our circle\n",
    "pi*r**2 # this gives the area of the circle"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "characteristic-logging",
   "metadata": {},
   "source": [
    "As you saw here, numbers can be raised to a power using `**`. **Important**: `^` will *not* work as expected, it is another operator. Most of the time, we recommend doing this instead to clear up any confusion:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "developed-findings",
   "metadata": {},
   "outputs": [],
   "source": [
    "pi*math.pow(r, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "simplified-limit",
   "metadata": {},
   "source": [
    "Just as in math, functions can have multiple arguments. For example, if you want to round a number to the third decimal place, you can do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "young-shock",
   "metadata": {},
   "outputs": [],
   "source": [
    "round(1.4567, 3)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
