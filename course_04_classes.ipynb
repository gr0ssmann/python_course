{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "executive-wages",
   "metadata": {},
   "source": [
    "# Classes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "extra-potter",
   "metadata": {},
   "source": [
    "Classes are blueprints for objects that are more complex than Python's built-in types such as `list`, `dict`, `int` and so on. For example, let's define a class for an inventory:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "analyzed-worse",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Inventory:\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "harmful-possession",
   "metadata": {},
   "source": [
    "(Note how we use [CamelCase](https://stackoverflow.com/a/12007958) for class names.)\n",
    "\n",
    "This allows us to do the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "processed-clear",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_books = Inventory()\n",
    "\n",
    "my_books"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "recent-copyright",
   "metadata": {},
   "source": [
    "`my_books` is now an `Inventory`. We call a particular realization of a class an *instance*. Hence, `my_books` is an instance of the class `Inventory`. By itself, that is not so useful. At the very least, when we create a new `Inventory`, we should initialize some internal state of that particular inventory. That is done by defining a *constructor*. In Python, constructors are defined as the class method `__init__`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "alike-masters",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Inventory:\n",
    "    def __init__(self, description):\n",
    "        self.data = []\n",
    "        self.description = description\n",
    "\n",
    "my_books = Inventory(\"Books\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "played-summer",
   "metadata": {},
   "source": [
    "Functions that begin and end with `__` are typically functions or methods that allow us to do \"special\" things according to the Python standard. You will see more examples soon. *Fundamentally, methods are the functions inside of a class. They are called as `instance.method()`, in which case `Class.method` is passed `instance` as the first argument. This first argument is traditionally called `self`.* For example,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "patient-address",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "class Inventory:\n",
    "    def __init__(self, description):\n",
    "        self.data = []\n",
    "        self.description = description\n",
    "\n",
    "    def do_something(self):\n",
    "        print(f\"The description is: {self.description}\")\n",
    "\n",
    "my_books = Inventory(\"Books\")\n",
    "\n",
    "my_books.do_something()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "running-syndrome",
   "metadata": {},
   "source": [
    "As you see, the method `do_something` can retrieve the `description` of a particular instance. Note that instances are separate from each other; they are like silos:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "engaging-manor",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_songs = Inventory(\"Songs\")\n",
    "\n",
    "my_books.do_something()\n",
    "my_songs.do_something()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "falling-basket",
   "metadata": {},
   "source": [
    "We can easily add more methods."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "liberal-sterling",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Inventory:\n",
    "    def __init__(self, description):\n",
    "        self.data = []\n",
    "        self.description = description\n",
    "    \n",
    "    def add(self, item):\n",
    "        self.data.append(item)\n",
    "    \n",
    "    def remove(self, item):\n",
    "        self.data.remove(item)\n",
    "    \n",
    "    def suggest(self):\n",
    "        import random\n",
    "        \n",
    "        return random.choice(self.data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "recorded-bermuda",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "my_books = Inventory(\"Books\")\n",
    "my_books.add(\"War and Peace\")\n",
    "my_books.add(\"Anthem\")\n",
    "my_books.add(\"Atlas Shrugged\")\n",
    "my_books.add(\"Deschooling Society\")\n",
    "\n",
    "print(my_books.suggest())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "going-print",
   "metadata": {},
   "source": [
    "It is best practice to define a `__str__` method for one's own classes. Note what happens if we just type this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "alpine-bunny",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(my_books)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "subtle-narrative",
   "metadata": {},
   "source": [
    "Let's do this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "desperate-montana",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Inventory:\n",
    "    def __init__(self, description):\n",
    "        self.data = []\n",
    "        self.description = description\n",
    "    \n",
    "    def __str__(self):\n",
    "        return f\"<Inventory: {self.description}, {len(self.data)} items>\"\n",
    "    \n",
    "    def add(self, item):\n",
    "        self.data.append(item)\n",
    "    \n",
    "    def remove(self, item):\n",
    "        self.data.remove(item)\n",
    "    \n",
    "    def suggest(self):\n",
    "        import random\n",
    "        \n",
    "        return random.choice(self.data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "defined-shopper",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_books = Inventory(\"Books\")\n",
    "my_books.add(\"Animal Farm\")\n",
    "\n",
    "print(my_books)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
